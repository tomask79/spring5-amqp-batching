package com.example.integration.demo.configurations;

import com.example.integration.demo.broker.EmbeddedInMemoryQpidBroker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Broker {

    @Bean(name = "aqmpBroker")
    public EmbeddedInMemoryQpidBroker broker() throws Exception {
        EmbeddedInMemoryQpidBroker embeddedInMemoryQpidBroker =
                new EmbeddedInMemoryQpidBroker();

        embeddedInMemoryQpidBroker.start();

        return embeddedInMemoryQpidBroker;
    }
}
