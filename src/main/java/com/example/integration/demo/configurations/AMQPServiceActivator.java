package com.example.integration.demo.configurations;

import java.util.List;

public class AMQPServiceActivator {

    public void processBatch(List<String> messageList) {
        System.out.println("Doing in receiver ");
        for (String messagePayload: messageList) {
            System.out.println("[PAYLOAD]: " + messagePayload);
        }
    }
}
