package com.example.integration.demo.configurations;

import org.springframework.amqp.rabbit.batch.BatchingStrategy;
import org.springframework.amqp.rabbit.batch.SimpleBatchingStrategy;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.BatchingRabbitTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;

@Configuration
public class AMQPTemplateConfiguration {

    @Bean(name="batchingRabbitTemplate")
    public BatchingRabbitTemplate batchingRabbitTemplate(ConnectionFactory connectionFactory) {
        BatchingStrategy strategy = new SimpleBatchingStrategy(5, 2000, 15_000);
        TaskScheduler scheduler = new ConcurrentTaskScheduler();

        BatchingRabbitTemplate template = new BatchingRabbitTemplate(strategy, scheduler);
        template.setConnectionFactory(connectionFactory);
        template.setExchange("inputDirect");
        return template;
    }
}
