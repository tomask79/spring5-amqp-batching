package com.example.integration.demo.configurations;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.batch.SimpleBatchingStrategy;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.amqp.inbound.AmqpInboundChannelAdapter;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

@Configuration
public class AMQPEndpointConfiguration {

    public static final String queueName = "inputQueue";

    @Bean
    public MessageChannel amqpInputChannel() {
        return new DirectChannel();
    }

    @Bean
    public SimpleMessageListenerContainer container(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container =
                new SimpleMessageListenerContainer(connectionFactory);
        container.setQueueNames(queueName);
        container.setConcurrentConsumers(2);
        container.setConsumerBatchEnabled(true);
        return container;
    }

    @Bean
    public DirectExchange inputDirect() {
        return new DirectExchange("inputDirect");
    }

    @Bean
    public Binding binding1a(DirectExchange inputDirect, Queue inputQueue) {
        return BindingBuilder.bind(inputQueue)
                .to(inputDirect)
                .with("inputKey");
    }

    @Bean
    public Queue inputQueue() {
        return new Queue(queueName, false);
    }

    @Bean
    public AmqpInboundChannelAdapter inbound(SimpleMessageListenerContainer listenerContainer,
                                             @Qualifier("amqpInputChannel") MessageChannel channel) {
        AmqpInboundChannelAdapter adapter = new AmqpInboundChannelAdapter(listenerContainer);
        adapter.setOutputChannel(channel);
        adapter.setBatchMode(AmqpInboundChannelAdapter.BatchMode.EXTRACT_PAYLOADS);
        return adapter;
    }

    @Bean
    @ServiceActivator(inputChannel = "amqpInputChannel")
    public AMQPServiceActivator handler() {
        return new AMQPServiceActivator();
    }
}
