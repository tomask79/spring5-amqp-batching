package com.example.integration.demo.sender;

import com.example.integration.demo.configurations.AMQPEndpointConfiguration;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.batch.BatchingStrategy;
import org.springframework.amqp.rabbit.batch.SimpleBatchingStrategy;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.BatchingRabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;

import javax.annotation.Resource;

@Configuration
public class AMQPSender {

    @Resource(name="batchingRabbitTemplate")
    private BatchingRabbitTemplate batchingRabbitTemplate;

    @Scheduled(fixedDelay = 3000)
    public void runSending() {
        MessageProperties props = new MessageProperties();
        String payload = "Test at: "+new java.util.Date();
        System.out.println("Sending "+payload);
        batchingRabbitTemplate.convertAndSend("inputKey", payload);
    }
}
